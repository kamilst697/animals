public class Lion extends Animal implements IMeatable{
    public Lion(String species, String name) {
        super(species, name);
    }


    @Override
    public void eatingMeat() {
        System.out.println("Lew je mięso");
    }

    @Override
    public void makeNoise() {
        System.out.println("Lew robi hałas");
    }
}
