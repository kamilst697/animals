public class Vet<T extends Animal> {
    private final T animal;

    public Vet(T animal) {
        this.animal = animal;
    }

    public String getName() {
        return animal.getName();
    }

    public T getAnimal() {
        return animal;
    }
}
