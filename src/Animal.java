public abstract class Animal{
    private String species;
    private String name;

    public Animal(String species, String name) {
        this.species = species;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void makeNoise();
}
