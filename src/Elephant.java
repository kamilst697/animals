public class Elephant extends Animal{
    public Elephant(String species, String name) {
        super(species, name);
    }

    @Override
    public void makeNoise() {
        System.out.println("Słoń mówi");
    }
}
