public class Fish extends Animal{
    public Fish(String species, String name) {
        super(species, name);
    }

    @Override
    public void makeNoise() {
        System.out.println("Plum plum");
    }
}
