public class Wolf extends Animal implements IMeatable{
    public Wolf(String species, String name) {
        super(species, name);
    }

    @Override
    public void eatingMeat() {
        System.out.println("Wilk je mięso");
    }

    @Override
    public void makeNoise() {
        System.out.println("Woof Woof");
    }
}
