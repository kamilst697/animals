import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Vet<? extends Animal>> vetList = createVetList();
        List<IMeatable> meatableList = new ArrayList<>();
        for (Vet vet : vetList) {
            System.out.println(vet.getAnimal().getName());
            if (vet.getAnimal() instanceof IMeatable) {
                meatableList.add((IMeatable) vet.getAnimal());
            }
        }

        for (IMeatable meatable : meatableList) {
            meatable.eatingMeat();
        }
//        List<IMeatable> meatablesAnimalsList = createMeatableAnimalList();
//        for (IMeatable meatable : meatablesAnimalsList) {
//            if (meatable instanceof Animal) {
//                System.out.println(((Animal) meatable).getName());
//            }
//        }


    }

    private static List<Vet<? extends Animal>> createVetList() {
        return new ArrayList() {{
            add(new Vet<>(new Lion("mammal", "lion")));
            add(new Vet<>(new Wolf("mammal", "wolf")));
            add(new Vet<>(new Fish("mammal", "fish")));
            add(new Vet<>(new Elephant("mammal", "elephant")));
        }};
    }

//    private static List<IMeatable> createMeatableAnimalList() {
//        return new ArrayList() {{
//           add(new Lion("mammal", "lion"));
//           add(new Wolf("mammal", "wolf"));
//           add(new testClass());
//        }};
//    }
}

class testClass implements IMeatable{

    @Override
    public void eatingMeat() {

    }
}

